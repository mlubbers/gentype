definition module Types

from Data.Either import :: Either
from GenType import :: Box, :: GType, generic gType
from GenType.CSerialise import generic gCSerialise, generic gCDeserialise, :: CDeserialiseError

derive gType T, R, Frac, Tr, Fix, Odd, Even, SR, List, Enum, NT, Blurp, EnumList, ER, CP, RA, Nest, MR, P, UInt8, UInt16, Int8, Int16, Either
derive gCSerialise CP, NT, ER, Enum, RA, UInt8, UInt16, Int8, Int16, Odd, Even, Either, P
derive gCDeserialise CP, NT, ER, Enum, RA, UInt8, UInt16, Int8, Int16, Odd, Even, Either, P
uint8Typedef :: [String]
uint8Parser  :: String -> [String]
uint8Printer :: String -> [String]
uint16Typedef :: [String]
uint16Parser  :: String -> [String]
uint16Printer :: String -> [String]
int8Typedef :: [String]
int8Parser  :: String -> [String]
int8Printer :: String -> [String]
int16Typedef :: [String]
int16Parser  :: String -> [String]
int16Printer :: String -> [String]

//:: P m = P (Tr m Int) | P2 (m Bool Bool)
:: P m a = P (Tr m) | P2 (m a)
:: T a = T2 a Char
:: NT =: NT Int
//:: SR = {f1 :: Int, f2 :: Bool, f3 :: Tr Either Bool, f4 :: Enum}
:: SR = {f1 :: Int, f2 :: Bool, f3 :: Tr (?), f4 :: Enum}
:: R a = {f1 :: ? (R a), f2 :: Bool, f3 :: T a, f4 :: Char -> Int,
	f5 :: [([#Int], [#Int!], [!Int!], [!Int], [Int!])],
	f6 :: ({!Int}, {R Bool}, {#Char}, {32#Int}),/*({!Int}, {#Char}, {R Bool})*/
	f7 :: {!Int}}
//:: Tr m b = Tr (m Int b) | TrBork
:: Tr m = Tr (m Bool) | TrBork
:: Frac a = (/.) infixl 7 a a  | Flurp
:: Fix f = Fix (f (Fix f))
:: List a = Cons a (List a) | Nil
:: Blurp a = Blurp (List a) | Blorp
:: EnumList = ECons Enum EnumList | ENil
:: ER = {nat :: Int, boolean :: Bool}
:: RA a = {a1 :: a, a2 :: Int}
:: MR m = {b1 :: m Int}
:: CP
	= CLeft (RA Char) [Bool] Int ()
	| CRight Enum (ER, NT)
	| CArr {Int} {Bool} UInt8 UInt16 Int8 Int16 Real
	| CMutrec (Odd Int)
//	| CTr (Tr (?)) (P (Either ()) Char)
	| CTr (Tr (?)) (P (?) Char)
:: Odd a = OddBlurp | Odd (Even a)
:: Even a = EvenBlurp | Even (Odd a)
:: Enum = A | B | C
:: Nest m = Nest (m (m (m Int))) | NestBlurp
:: UInt8 =: UInt8 Int
:: UInt16 =: UInt16 Int
:: Int8 =: Int8 Int
:: Int16 =: Int16 Int

cp :: Box GType CP
