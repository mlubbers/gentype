module runtests

import StdEnv, StdDebug
import StdOverloadedList

import Control.GenBimap
import Data.Either
import Data.Func
import Data.Functor
import Data.Int
import Data.Tuple
import Data.Maybe
import Gast
import Gast.Gen
import System._Pointer
import System._Posix
import Text
import Text.GenPrint

import GenType.CSerialise

import Types

import code from "cp.o"
import code from "cruntests.o"

derive gEq CP, NT, ER, Enum, RA, UInt8, UInt16, Int8, Int16, Odd, Even, Tr, P
derive class Gast CP, NT, ER, Enum, RA, Tr, ?, P
derive genShow UInt8, UInt16, Int8, Int16, Odd, Even
derive gPrint UInt8, UInt16, Int8, Int16, Odd, Even
ggen{|UInt8|} s = [!UInt8 i\\i<-[0, 255:[1..254]]]
ggen{|UInt16|} s = [!UInt16 i\\i<-[0, 65535:[1..65534]]]
ggen{|Int8|} s = [!Int8 i\\i<-[-128,127:[-127..126]]]
ggen{|Int16|} s = [!Int16 i\\i<-[-32768, 32767:[-32767..32765]]]
ggen{|Odd|} a s = [!OddBlurp,Odd EvenBlurp,Odd (Even (OddBlurp))]
ggen{|Even|} a s = [!EvenBlurp,Even OddBlurp,Even (Odd (EvenBlurp))]

ppUInt8 :: !Int !String !*World -> *(!Pointer, !*World)
ppUInt8 _ _ _ = code { ccall pp_UInt8 "Is:p:A" }
ppInt16 :: !Int !String !*World -> *(!Pointer, !*World)
ppInt16 _ _ _ = code { ccall pp_Int16 "Is:p:A" }
ppInt8 :: !Int !String !*World -> *(!Pointer, !*World)
ppInt8 _ _ _ = code { ccall pp_Int8 "Is:p:A" }
ppUInt16 :: !Int !String !*World -> *(!Pointer, !*World)
ppUInt16 _ _ _ = code { ccall pp_UInt16 "Is:p:A" }
ppInt :: !Int !String !*World -> *(!Pointer, !*World)
ppInt _ _ _ = code { ccall pp_Int "Is:p:A" }
ppBool :: !Int !String !*World -> *(!Pointer, !*World)
ppBool _ _ _ = code { ccall pp_Bool "Is:p:A" }
ppChar :: !Int !String !*World -> *(!Pointer, !*World)
ppChar _ _ _ = code { ccall pp_Char "Is:p:A" }
pp_Unit :: !Int !String !*World -> *(!Pointer, !*World)
pp_Unit _ _ _ = code { ccall pp__Unit "Is:p:A" }
ppEnum :: !Int !String !*World -> *(!Pointer, !*World)
ppEnum _ _ _ = code { ccall pp_Enum "Is:p:A" }
ppER :: !Int !String !*World -> *(!Pointer, !*World)
ppER _ _ _ = code { ccall pp_ER "Is:p:A" }
ppNT :: !Int !String !*World -> *(!Pointer, !*World)
ppNT _ _ _ = code { ccall pp_NT "Is:p:A" }
ppCP :: !Int !String !*World -> *(!Pointer, !*World)
ppCP _ _ _ = code { ccall pp_CP "Is:p:A" }
ppReal :: !Int !String !*World -> *(!Pointer, !*World)
ppReal _ _ _ = code { ccall pp_Real "Is:p:A" }

ppTupleBoolBool :: !Int !String !*World -> *(!Pointer, !*World)
ppTupleBoolBool _ _ _ = code { ccall pp__TupleBoolBool "Is:p:A" }

ppArrayBool :: !Int !String !*World -> *(!Pointer, !*World)
ppArrayBool _ _ _ = code { ccall pp_Bool_Array "Is:p:A" }

runTests :: !(Int String *World -> *(Pointer, *World)) ![!a] !*World -> *World | gCSerialise{|*|}, gCDeserialise{|*|}, Gast, gEq{|*|} a
runTests pp els w = foldl ffun w (take 10000 [e\\e<|-els])
where
	//Needs strictness to not overflows heap
	ffun :: !*World !a -> *World | gCSerialise{|*|}, gCDeserialise{|*|}, Gast, gEq{|*|} a
	ffun w el
		# msg = toString (gCSerialise{|*|} el [])
		# (p, w) = pp (size msg) msg w
		# newmsg = derefCharArray p (size msg)
		| newmsg <> msg = abort
			(   "Unequal serialised values for: " +++ printToString el +++ "\n"
			+++ "gave: " +++ join ", " [toString (toInt i)\\i<-:msg] +++ "\n"
			+++ "got:  " +++ join ", " [toString (toInt i)\\i<-:newmsg] +++ "\n"
			)
		= case gCDeserialise{|*|} listTop [toInt c\\c<-:newmsg] of
			Left e = abort ("Error deserialising: " +++ toString e +++ "\n")
			Right (a, [])
				| a === el = w
				= abort
					(   "Unequal deserialised values\n"
					+++ "gave:  " +++ printToString el +++ "\n"
					+++ "got: " +++ printToString a +++ "\n"
					)
			Right _ = abort
					(   "Input exhausted: \n"
					+++ "val: " +++ printToString el +++ "\n"
					+++ "got: " +++ printToString newmsg +++ "\n"
					)

testMultiple world
	| not ((deserialiseMultiple listTop [] [0,42,0])
		=: (Right ([UInt16 42], [0]))) = abort "Fail testMultiple"
	| not ((deserialiseMultiple listTop [] [0,42])
		=: (Right ([UInt16 42], []))) = abort "Fail testMultiple"
	| not ((deserialiseMultiple listTop [] [0,42,0,38,0])
		=: (Right ([UInt16 42,UInt16 38], [0]))) = abort "Fail testMultiple"
	| not ((deserialiseMultiple listTop [] [0,42,0,38])
		=: (Right ([UInt16 42,UInt16 38], []))) = abort "Fail testMultiple"
	| not ((deserialiseMultiple listTop (tl [0]) (tl [0]))
		=: (Right ([], []))) = abort "Fail testMultiple"
	= world

Start w = id
	$ runTests ppUInt8 [!UInt8 0:ggen{|*|} genState]
	$ runTests ppUInt16 [!UInt16 0:ggen{|*|} genState]
	$ runTests ppBool [!True:ggen{|*|} genState]
	$ runTests ppInt [!0:ggen{|*|} genState]
	$ runTests ppChar [!'\0':ggen{|*|} genState]
	$ runTests ppReal (Filter (not o isNaN) [!0.0:ggen{|*|} genState])
	$ runTests pp_Unit [!():ggen{|*|} genState]
	$ runTests ppEnum [!A:ggen{|*|} genState]
	$ runTests ppER [!{nat=1,boolean=True}:ggen{|*|} genState]
	$ runTests ppNT [!NT 42:ggen{|*|} genState]
	$ runTests ppTupleBoolBool [!(True, True):ggen{|*|} genState]
	$ runTests ppArrayBool [!{!True}:ggen{|*|} genState]
	$ runTests ppCP (Filter notnan [!CLeft {a1='\0',a2=5} [True] 0 ():ggen{|*|} genState])
	$ testMultiple
	$ w

notnan (CArr _ _ _ _ _ _ x) = x == x
notnan _ = True
