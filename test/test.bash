#!/bin/bash
set -e

CLM="${CLM:-clm}"
CLMFLAGS="${CLMFLAGS:- -tst -s 2M -h 10M -desc -exl}"
CLMLIBS="${CLMLIBS:- -IL Platform -IL Gast} -I ../src"

CFLAGS="${CFLAGS:- -Wall -Wextra -std=c99 -Werror}"

$CLM $CLMLIBS $CLMFLAGS test -o test
./test
gcc $CFLAGS -c *.c
mv *.o Clean\ System\ Files
$CLM $CLMLIBS $CLMFLAGS runtests -o runtests
./runtests
