#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>

#include "cp.h"

uint8_t rmsg[1000];
uint8_t *msg;
int idx, msglen;

uint8_t get(void)
{
	if (idx == msglen) {
		fprintf(stderr, "Input exhausted\n");
		exit(EXIT_FAILURE);
	}
	return msg[idx++];
}

void put(uint8_t c)
{
	if (idx == msglen) {
		fprintf(stderr, "Buffer overrun\n");
		exit(EXIT_FAILURE);
	}
	msg[idx++] = c;
}

#define pp_(name, type, parse, print)\
uint8_t *name(int sz, uint8_t *newmsg) {\
	msg = newmsg;\
	idx = 0;\
	msglen = sz;\
	type r = parse(get, malloc);\
	idx = 0;\
	msg = &rmsg[0];\
	print(put, r);\
	return msg;\
}

pp_(pp_UInt8, UInt8, parse_UInt8, print_UInt8);
pp_(pp_UInt16, UInt16, parse_UInt16, print_UInt16);
pp_(pp_Int, Int, parse_Int, print_Int);
pp_(pp_Bool, Bool, parse_Bool, print_Bool);
pp_(pp_Char, Char, parse_Char, print_Char);
pp_(pp_Enum, enum Enum, parse_Enum, print_Enum);
pp_(pp_Real, Real, parse_Real, print_Real);
pp_(pp__Unit, enum _Unit, parse__Unit, print__Unit);
pp_(pp_NT, NT, parse_NT, print_NT);
pp_(pp_ER, struct ER, parse_ER, print_ER);
pp_(pp_CP, struct CP, parse_CP, print_CP);
pp_(pp_Bool_Array, struct Bool_Array, parse_Bool_Array, print_Bool_Array);

uint8_t *pp__TupleBoolBool(int sz, uint8_t *newmsg) {
	msg = newmsg;
	idx = 0;
	msglen = sz;
	struct _Tuple2 r = parse__Tuple2(get, malloc, parse_Bool_p, parse_Bool_p);
	idx = 0;
	msg = &rmsg[0];
	print__Tuple2(put, r, print_Bool_p, print_Bool_p);
	return msg;
}
