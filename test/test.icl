module test

import StdEnv
import Text
import Data.Either

import GenType
import GenType.CCode
import Types

writeToFile :: [String] String *World -> *World
writeToFile data fp w
	# (ok, f, w) = fopen fp FWriteText w
	| not ok = abort ("Couldn't open " +++ fp +++ " for writing\n")
	# f = foldl (flip fwrites) f data
	# (ok, w) = fclose f w
	| not ok = abort ("Couldn't close " +++ fp +++ "\n")
	= w

Start w = case generateCCode opts cp of
	Left e = abort ("Error generating code: " +++ e)
	Right (h, c) = writeToFile h "cp.h" (writeToFile c "cp.c" w)
where
	opts = {zero & basename="cp", overrides=
		[(gTypeName (gTypeForValue (UInt8 0)), uint8Typedef, uint8Parser, uint8Printer)
		,(gTypeName (gTypeForValue (UInt16 0)), uint16Typedef, uint16Parser, uint16Printer)
		,(gTypeName (gTypeForValue (Int8 0)), int8Typedef, int8Parser, int8Printer)
		,(gTypeName (gTypeForValue (Int16 0)), int16Typedef, int16Parser, int16Printer)
		]}
