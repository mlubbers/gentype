definition module GenType

import _SystemStrictLists
import _SystemStrictMaybes
from Data.Generics import class genericDescriptorName
import StdGeneric
from StdOverloaded import class ==, class toString

//* Auxiliary type to help with casting values, this is gone at runtime
:: Box b a =: Box b
derive bimap Box
unBox (Box b) :== b
box b :== Box b
reBox x :== box (unBox x)

//* Deeply embedded generic type representation
:: GType
	= GTyBasic BasicType
	| GTyRef String
	| GTyArrow GType GType
	| GTyArray ArrayType GType
	| GTyUList UListType GType
	| GTyUMaybe GType
	| GTyUnit
	| GTyEither GType GType
	| GTyPair GType GType
	| GTyCons GenericConsDescriptor GType
	| GTyField GenericFieldDescriptor GType
	| GTyObject GenericTypeDefDescriptor GType
	| GTyRecord GenericRecordDescriptor GType

//* Type representation larded with the generic type information
:: Type
	= TyBasic BasicType
	| TyRef String
	| TyArrow Type Type
	| TyArray ArrayType Type
	| TyUList UListType Type
	| TyUMaybe Type
	| TyNewType GenericTypeDefDescriptor GenericConsDescriptor Type
	| TyObject GenericTypeDefDescriptor [(GenericConsDescriptor, [Type])]
	| TyRecord GenericRecordDescriptor [(GenericFieldDescriptor, Type)]

//* Basic types
:: BasicType = BTInt | BTChar | BTReal | BTBool | BTDynamic | BTFile | BTWorld
//* Array kinds
:: ArrayType = AStrict | ALazy | AUnboxed | APacked
//* Unboxed list kinds
:: UListType = ULLazy | ULStrict
//* Kind of a type
:: Kind = KStar | (KArrow) infixr 1 Kind Kind

instance == GType, Type, BasicType, ArrayType, UListType, GenType, Kind
instance toString GType, Type, BasicType, ArrayType, UListType, Kind, GenType
instance genericDescriptorName ArrayType

/**
 * Removes recursive types by replacing them with references
 *
 * @param gtype
 * @result the main type
 * @result the separated types grouped in strongly connected components
 */
flattenGType :: GType -> [[GType]]

//* Convert a GType to a Type
gTypeToType :: GType -> Type

//* Extract the name of the type
typeName :: Type -> String

//* Extract the genType for the entire type
typeGenType :: Type -> GenType

//* Extract the genType for all constructors of a type
typeConstructors :: Type -> [GenType]

//* Generate a gtype for a value
gTypeForValue :: a -> GType | gType{|*|} a

//* Extract the kind of the type's constructors (see `{{typeGenType}}`)
genTypeKind :: [GenType] -> Kind

//* @type Type -> Kind
typeKind t :== genTypeKind (typeConstructors t)

//* Get the type of a gtype
//* @type GType -> String
gTypeName b :== typeName (gTypeToType b)

//* Predicate whether the outer type is a builtin type
class isBuiltin a :: a -> Bool
instance isBuiltin Type, GType

//* Predicate whether the outer type is a basic type
class isBasic a :: a -> Bool
instance isBasic Type, GType

//* Replace builtin constructors with their pretty names (e.g. _!Cons with [!])
class replaceBuiltins a :: a -> a
instance replaceBuiltins Type, GType, GenType

//* Creates a deep embedded generic representation of a type
generic gType a :: Box GType a
derive gType UNIT, EITHER, PAIR, CONS of gcd, FIELD of gfd, OBJECT of gtd, RECORD of grd
derive gType Int, Bool, Real, Char, World, File
derive gType (->)
derive gType ?, ?^
derive gType [], [! ], [ !], [!!], [#], [#!], {}, {!}, {#}, {32#}
derive gType (), (,), (,,), (,,,), (,,,,), (,,,,,), (,,,,,,), (,,,,,,,)
