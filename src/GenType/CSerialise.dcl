definition module GenType.CSerialise

from _SystemArray import class Array
import StdGeneric
from StdOverloaded import class toString, class toInt, class fromInt
from Data.Either import :: Either


generic gCSerialise a :: a -> ([Int] -> [Int])
derive gCSerialise UNIT, EITHER, PAIR, CONS of {gcd_type_def,gcd_index}, FIELD, OBJECT, RECORD
derive gCSerialise Int, Bool, Real, Char
derive gCSerialise ?, ?^
derive gCSerialise [], [! ], [ !], [!!], {}, {!}
derive gCSerialise (), (,), (,,), (,,,), (,,,,), (,,,,,), (,,,,,,), (,,,,,,,)

serialiseToFile :: a *File -> *File | gCSerialise{|*|} a

listTop :: [Int] -> Either CDeserialiseError (Int, [Int])
sfileTop :: File -> Either CDeserialiseError (Int, File)
:: CDeserialiseError = CDInputExhausted | CDUnknownConstructor String Int | CDUser String
instance toString CDeserialiseError
generic gCDeserialise a :: (st -> Either CDeserialiseError (Int, st)) -> (st -> Either CDeserialiseError (a, st))
derive gCDeserialise UNIT, EITHER, PAIR, CONS of {gcd_type_def,gcd_index}, FIELD, OBJECT, RECORD
derive gCDeserialise Int, Bool, Real, Char
derive gCDeserialise ?, ?^
derive gCDeserialise [], [! ], [ !], [!!], {}, {!}
derive gCDeserialise (), (,), (,,), (,,,), (,,,,), (,,,,,), (,,,,,,), (,,,,,,,)

deserialiseInt :: Int Bool (st -> Either CDeserialiseError (Int, st)) -> (st -> Either CDeserialiseError (a, st)) | fromInt a
serialiseInt :: Int Bool a -> [Int] | toInt a

/**
 * Deserialise size prefixed number of elements
 *
 * @param bogus value to easily force type checking
 * @param parse size
 * @param parse element
 * @param top
 * @result list parser
 */
deserialiseN ::
	a 
	((st -> Either CDeserialiseError (Int, st)) -> (st -> Either CDeserialiseError (a, st)))
	((st -> Either CDeserialiseError (Int, st)) -> (st -> Either CDeserialiseError (el, st)))
	(st -> Either CDeserialiseError (Int, st))
	-> (st -> Either CDeserialiseError ([el], st)) | toInt a

/**
 * Deserialise until the input is exhausted
 *
 * @param top
 * @param accumulator
 * @result list parser
 */
deserialiseMultiple :: (st -> Either CDeserialiseError (Int, st)) [a] st -> 
	Either CDeserialiseError ([a], st) | gCDeserialise{|*|} a
