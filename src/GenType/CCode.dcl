definition module GenType.CCode

from StdGeneric import :: GenericTypeDefDescriptor
from StdOverloaded import class zero
from Data.Either import :: Either

from GenType import :: Box, :: GType, :: Type, generic gType

:: GenCCodeOptions =
	{ basename   :: String
	, genParser  :: Bool
	, genPrinter :: Bool
	, customDeps :: [[Type]] -> [[Type]]
	, overrides  :: [(String, [String], String -> [String], String -> [String])]
	}
instance zero GenCCodeOptions

generateCCode :: GenCCodeOptions (Box GType a) -> Either String ([String], [String]) | gType{|*|} a

/**
 * Create a C-safe type name
 */
safe :: String -> String

/**
 * Return the C type prefix, e.g. struct, enum
 */
prefix :: Type -> String

/**
 * Return the C constructorname
 */
consName :: GenericTypeDefDescriptor -> String
