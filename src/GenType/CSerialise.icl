implementation module GenType.CSerialise

import Data.Array
import Data.Either
import StdEnv
import StdGeneric

put a c = [a:c]

gCSerialise{|UNIT|} _ = id
gCSerialise{|EITHER|} f _ (LEFT x) = f x
gCSerialise{|EITHER|} _ f (RIGHT x) = f x
gCSerialise{|PAIR|} fl fr (PAIR l r) = fl l o fr r
gCSerialise{|CONS of {gcd_type_def,gcd_index}|} f (CONS x)
	//Enum
	| and [c.gcd_arity == 0\\c<-gcd_type_def.gtd_conses]
		= put gcd_index o f x
	//Single constructor
	| gcd_type_def.gtd_num_conses == 1 = f x
	= put gcd_index o f x
gCSerialise{|FIELD|} f (FIELD x) = f x
//Newtypes automatically work
gCSerialise{|OBJECT|} f (OBJECT x) = f x
gCSerialise{|RECORD|} f (RECORD x) = f x
gCSerialise{|Int|} x = \c->serialiseInt (IF_INT_64_OR_32 8 4) True x ++ c
gCSerialise{|Bool|} x = put (if x 1 0)
gCSerialise{|Real|} x = \c->IF_INT_64_OR_32
	(toBytes 8 (convert_double_to_double_in_int x))
	(let (x1, x2) = convert_double_to_double_in_int32 x
	 in  toBytes 4 x1 ++ toBytes 4 x2)
	++ c

convert_double_to_double_in_int :: !Real -> Int
convert_double_to_double_in_int r = code { no_op }

convert_double_to_double_in_int32 :: !Real -> (!Int, !Int)
convert_double_to_double_in_int32 _ = code { no_op }

gCSerialise{|Char|} x = put (toInt x)
gCSerialise{|{}|} f x = serarr f x
gCSerialise{|{!}|} f x = serarr f x
serarr f x c = serialiseInt 4 False (size x) ++ foldrArr f c x

toBytes :: Int Int -> [Int]
toBytes bytes i = [i >> (b*8) bitand 0xff\\b<-reverse [0..bytes-1]]
to2comp :: Int Int -> Int
to2comp bits i = if (i < 0) (2 ^ bits + i) i

derive gCSerialise (), (,), (,,), (,,,), (,,,,), (,,,,,), (,,,,,,), (,,,,,,,)
derive gCSerialise [], [! ], [ !], [!!], ?, ?^

serialiseToFile :: a *File -> *File | gCSerialise{|*|} a
serialiseToFile t f = foldr fwritec f (map toChar (gCSerialise{|*|} t []))

listTop :: [Int] -> Either CDeserialiseError (Int, [Int])
listTop [] = Left CDInputExhausted
listTop [x:xs] = Right (x, xs)

sfileTop :: File -> Either CDeserialiseError (Int, File)
sfileTop f
	# (ok, c, f) = sfreadc f
	| not ok = Left CDInputExhausted
	= Right (toInt c, f)

instance toString CDeserialiseError
where
	toString CDInputExhausted = "Input exhausted"
	toString (CDUnknownConstructor s i) = "Unknown constructor in type " +++ s +++ " (" +++ toString i +++ ")"
	toString (CDUser e) = "User error: " +++ e

pure a c :== Right (a, c)

(<$>) infixl 4
(<$>) f a :== a >>= pure o f

(<*>) infixl 4
(<*>) l r :== l >>= \f->r >>= \a->pure (f a)

(<|>) infixl 3
(<|>) l r :== \c->case l c of
	Left CDInputExhausted = Left CDInputExhausted
	Left e = r c
	Right a = Right a

(>>=) infixl 1
(>>=) l r :== \c->case l c of
	Left e = Left e
	Right (a, c) = r a c

sequence [] = pure []
sequence [x:xs] = (\x xs->[x:xs]) <$> x <*> sequence xs

gCDeserialise{|UNIT|} top = pure UNIT
gCDeserialise{|EITHER|} fl fr top = LEFT <$> fl top <|> RIGHT <$> fr top
gCDeserialise{|PAIR|} fl fr top = PAIR <$> fl top <*> fr top
gCDeserialise{|CONS of {gcd_type_def,gcd_index}|} f top
	//Enumeration
	| and [c.gcd_arity == 0\\c<-gcd_type_def.gtd_conses]
		= top >>= \c->if (c == gcd_index) (CONS <$> f top) (\_->Left (CDUnknownConstructor gcd_type_def.gtd_name c))
	//Single constructor
	| gcd_type_def.gtd_num_conses == 1 = CONS <$> f top
	= top >>= \c->if (c == gcd_index) (CONS <$> f top) (\_->Left (CDUnknownConstructor gcd_type_def.gtd_name c))
gCDeserialise{|FIELD|} f top = (\x->FIELD x) <$> f top
gCDeserialise{|OBJECT|} f top = (\x->OBJECT x) <$> f top
gCDeserialise{|RECORD|} f top = RECORD <$> f top
gCDeserialise{|Int|} top = deserialiseInt (IF_INT_64_OR_32 8 4) True top
gCDeserialise{|Bool|} top = ((==)1) <$> top
gCDeserialise{|Real|} top = IF_INT_64_OR_32
	(convert_double_in_int_to_double <$> gCDeserialise{|*|} top)
	(curry convert_double_in_int_to_double32 <$> gCDeserialise{|*|} top <*> gCDeserialise{|*|} top)

convert_double_in_int_to_double :: !Int -> Real
convert_double_in_int_to_double r = code { no_op }

convert_double_in_int_to_double32 :: !(!Int, !Int) -> Real
convert_double_in_int_to_double32 _ = code {
		.d 0 2 r
			jmp _convert_double_in_int_to_double32_
		.o 0 2 r
			:_convert_double_in_int_to_double32_
	}

gCDeserialise{|Char|} top = toChar <$> top
gCDeserialise{|{}|} f top = (\a->{a\\a<-a}) <$> deserialiseN 0 (deserialiseInt 4 False) f top
gCDeserialise{|{!}|} f top = (\a->{a\\a<-a}) <$> deserialiseN 0 (deserialiseInt 4 False) f top

fro2comp :: Int Int -> Int
fro2comp bits i = let mask = 2 ^ dec bits in ~(i bitand mask) + (i bitand bitnot mask)

fromBytes :: [Int] -> Int
fromBytes s = sum [toInt c << (b*8)\\c<-s & b<-reverse [0..length s - 1]]

deserialiseInt :: Int Bool (st -> Either CDeserialiseError (Int, st)) -> (st -> Either CDeserialiseError (a, st)) | fromInt a
deserialiseInt bytes twocomp top
	= fromInt o if twocomp (fro2comp (bytes*8)) id o fromBytes <$> sequence [top\\p<-[0..bytes-1]]

serialiseInt :: Int Bool a -> [Int] | toInt a
serialiseInt bytes twocomp num
	= toBytes bytes (if twocomp (to2comp (bytes*8)) id (toInt num))

derive gCDeserialise (), (,), (,,), (,,,), (,,,,), (,,,,,), (,,,,,,), (,,,,,,,)
derive gCDeserialise [], [! ], [ !], [!!], ?, ?^

deserialiseN ::
	a
	((st -> Either CDeserialiseError (Int, st)) -> (st -> Either CDeserialiseError (a, st)))
	((st -> Either CDeserialiseError (Int, st)) -> (st -> Either CDeserialiseError (el, st)))
	(st -> Either CDeserialiseError (Int, st))
	-> (st -> Either CDeserialiseError ([el], st)) | toInt a
deserialiseN _ psize pel top = psize top >>= \sz->sequence [pel top\\p<-[0..toInt sz-1]]

deserialiseMultiple :: (st -> Either CDeserialiseError (Int, st)) [a] st ->
	Either CDeserialiseError ([a], st) | gCDeserialise{|*|} a
deserialiseMultiple top acc st = case gCDeserialise{|*|} top st of
	Right (el, st) = deserialiseMultiple top [el:acc] st
	Left CDInputExhausted = Right (reverse acc, st)
	Left e = Left e
