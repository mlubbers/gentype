implementation module GenType.CCode

import StdEnv
import Data.Either
import Data.Func
from Data.Map import :: Map(..), fromList, get
from Text import class Text(concat,toUpperCase), instance Text String
import Data.Maybe
import Control.Monad

import GenType
import GenType.CCode.CType
import GenType.CCode.CParser
import GenType.CCode.CPrinter

instance zero GenCCodeOptions
where
	zero =
		{ basename   = ""
		, genParser  = True
		, genPrinter = True
		, customDeps = id
		, overrides  = []
		}

generateCCode :: GenCCodeOptions (Box GType a) -> Either String ([String], [String]) | gType{|*|} a
generateCCode opts box
	=         typedefs typedefMap type
	>>= \tdh->if opts.genParser (parsers parseMap type) (Right ([], []))
	>>= \(pah, pac)->if opts.genPrinter (printers printMap type) (Right ([], []))
	>>= \(prh, prc)->Right
		( flatten [toph, tdh, pah, prh, both]
		, flatten $ if (opts.genPrinter || opts.genParser)
			[[includes:topc], pac, prc, ["\n"]]
			[["\n"]]
		)
where
	both = ["#endif /* ", toUpperCase (safe opts.basename), "_H */\n"]
	toph =
		[ "#ifndef ", toUpperCase (safe opts.basename), "_H\n"
		, "#define ", toUpperCase (safe opts.basename), "_H\n"
		, includes]
	topc =
		[ "#include \"", opts.basename, ".h\"\n"
		, "union floatint {uint32_t intp; float floatp; };\n"
		, "union doubleint {uint64_t intp; double doublep; };\n"
		]
	includes = "#include <stdint.h>\n#include <stdbool.h>\n#include <stddef.h>\n"
	typedefMap = fromList [(n, t)\\(n, t, _, _)<-opts.overrides]
	parseMap = fromList [(n, p)\\(n, _, p, _)<-opts.overrides]
	printMap = fromList [(n, p)\\(n, _, _, p)<-opts.overrides]
	type = opts.customDeps $ map (map gTypeToType) $ flattenGType $ unBox box

safe :: String -> String
safe s = concat [fromMaybe {c} $ get c cs\\c <-:s]

cs = fromList
	[('~', "Tld"), ('@', "At"), ('#', "Hsh"), ('$', "Dlr"), ('%', "Prc")
	,('^', "Hat"), ('?', "Qtn"), ('!', "Bng"), (':', "Cln"), ('+', "Pls")
	,('-', "Min"), ('*', "Ast"), ('<', "Les"), ('>', "Gre"), ('\\', "Bsl")
	,('/', "Slh"), ('|', "Pip"), ('&', "Amp"), ('=', "Eq"), ('.', "Dot")
	,('{', "Cbo"), ('}', "Cbc")]

prefix :: Type -> String
prefix (TyRecord _ _) = "struct "
prefix (TyArray _ _) = "struct "
prefix (TyObject _ fs)
	| and [t =: [] \\ (_, t)<-fs] = "enum "
	| fs =: [(_, [_])] = ""
	| fs =: [_] = "struct "
	= "struct "
prefix  _ = ""

consName :: GenericTypeDefDescriptor -> String
consName s = "enum " +++ safe s.gtd_name
