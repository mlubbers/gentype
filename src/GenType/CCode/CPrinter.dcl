definition module GenType.CCode.CPrinter

from Data.Either import :: Either
from GenType import :: Type
from Data.Map import :: Map

/**
 * Generate a single printer for a type.
 * This does not terminate for a recursive type
 */
flatPrinter :: (Map String (String -> [String])) Type -> Either String ([String], [String])

/**
 * generate printers for the types grouped by strongly connected components
 */
printers :: (Map String (String -> [String])) [[Type]] -> Either String ([String], [String])
