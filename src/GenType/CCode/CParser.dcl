definition module GenType.CCode.CParser

from Data.Either import :: Either
from GenType import :: Type
from Data.Map import :: Map

/**
 * Generate a single parser for a type.
 * This does not terminate for a recursive type
 */
flatParser :: (Map String (String -> [String])) Type -> Either String ([String], [String])

/**
 * generate parsers for the types grouped by strongly connected components
 */
parsers :: (Map String (String -> [String])) [[Type]] -> Either String ([String], [String])
